# Generated by Django 3.1.3 on 2021-02-06 14:10

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Album',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255, verbose_name='Заголовок')),
                ('slug', models.SlugField(unique=True, verbose_name='Ссылка')),
                ('image', models.ImageField(upload_to='albums/', verbose_name='Картинка')),
                ('content', models.TextField(verbose_name='Описание')),
                ('date', models.DateField(verbose_name='Дата выпуска')),
                ('created_at', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Дата добавления на сайт')),
            ],
            options={
                'verbose_name': 'Альбом',
                'verbose_name_plural': 'Альбомы',
            },
        ),
        migrations.CreateModel(
            name='Audio',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255, verbose_name='Заголовок')),
                ('audio', models.FileField(upload_to='audio/files/', verbose_name='Аудио')),
                ('image', models.ImageField(blank=True, null=True, upload_to='audio/images/', verbose_name='Картинка')),
                ('date', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Дата добавления на сайт')),
            ],
            options={
                'verbose_name': 'Аудио',
                'verbose_name_plural': 'Аудио',
            },
        ),
        migrations.CreateModel(
            name='Genre',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255, verbose_name='Жанр')),
                ('slug', models.SlugField(unique=True, verbose_name='Ссылка')),
            ],
            options={
                'verbose_name': 'Жанр',
                'verbose_name_plural': 'Жанры',
            },
        ),
        migrations.CreateModel(
            name='View',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateTimeField(default=django.utils.timezone.now)),
                ('album', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='audio.album', verbose_name='Альбом')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='Пользователь')),
            ],
        ),
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('avatar', models.ImageField(null=True, upload_to='user/', verbose_name='Аватар')),
                ('phone', models.CharField(max_length=255, null=True, verbose_name='Телефон')),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='Пользователь')),
            ],
        ),
        migrations.CreateModel(
            name='Musician',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, verbose_name='Имя')),
                ('age', models.DateField(verbose_name='Дата рождения')),
                ('photo', models.ImageField(upload_to='musicians/', verbose_name='Фотография')),
                ('genres', models.ManyToManyField(related_name='musicians', to='audio.Genre', verbose_name='Жанры')),
            ],
            options={
                'verbose_name': 'Музыкант',
                'verbose_name_plural': 'Музыканты',
            },
        ),
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('text', models.TextField(verbose_name='Текст комментария')),
                ('date', models.DateField(default=django.utils.timezone.now, verbose_name='Дата')),
                ('album', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='audio.album', verbose_name='Альбом')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='Пользователь')),
            ],
            options={
                'verbose_name': 'Комментарий',
                'verbose_name_plural': 'Коментарии',
            },
        ),
        migrations.AddField(
            model_name='album',
            name='audios',
            field=models.ManyToManyField(related_name='albums', to='audio.Audio', verbose_name='Аудио'),
        ),
        migrations.AddField(
            model_name='album',
            name='genres',
            field=models.ManyToManyField(related_name='albums', to='audio.Genre', verbose_name='Жанры'),
        ),
        migrations.AddField(
            model_name='album',
            name='musicians',
            field=models.ManyToManyField(related_name='albums', to='audio.Musician', verbose_name='Музыканты'),
        ),
    ]
