from django.contrib import admin
from .models import Genre, Album, Comment, Audio, Profile, Playlist

class SlugAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}

admin.site.register(Genre, SlugAdmin)
admin.site.register(Album, SlugAdmin)
admin.site.register(Comment)
admin.site.register(Audio)
admin.site.register(Profile)
admin.site.register(Playlist)
