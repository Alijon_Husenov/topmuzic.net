from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from django.shortcuts import reverse


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, verbose_name="Пользователь")
    avatar = models.ImageField('Аватар', upload_to="user/", null=True)
    phone = models.CharField('Телефон', max_length=255, null=True)

class Genre(models.Model):
    title = models.CharField("Жанр", max_length=255)
    slug = models.SlugField('Ссылка', unique=True)

    class Meta:
        verbose_name = "Жанр"
        verbose_name_plural = "Жанры"

    def get_absolute_url(self):
        return reverse('genre_detail_url', kwargs={'slug': self.slug})

    def __str__(self):
        return self.title


class Audio(models.Model):
    title = models.CharField('Заголовок', max_length=255)
    audio = models.FileField('Аудио', upload_to="audio/files/")
    image = models.ImageField('Картинка', upload_to="audio/images/", null=True, blank=True)
    date = models.DateTimeField('Дата добавления на сайт', default=timezone.now)

    class Meta:
        verbose_name = "Аудио"
        verbose_name_plural = "Аудио"

    def __str__(self):
        return self.title


class Album(models.Model):
    title = models.CharField('Заголовок', max_length=255)
    slug = models.SlugField('Ссылка', unique=True)
    image = models.ImageField('Картинка', upload_to="albums/")
    genres = models.ManyToManyField(Genre, verbose_name="Жанры", related_name="albums")
    audios = models.ManyToManyField(Audio, verbose_name="Аудио", related_name="albums")
    content = models.TextField('Описание')
    date = models.DateField('Дата выпуска')
    created_at = models.DateTimeField('Дата добавления на сайт', default=timezone.now)

    class Meta:
        verbose_name = "Альбом"
        verbose_name_plural = "Альбомы"

    def get_absolute_url(self):
        return reverse('album_detail_url', kwargs={'slug': self.slug})

    def __str__(self):
        return self.title

class Comment(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name="Пользователь")
    album = models.ForeignKey(Album, on_delete=models.CASCADE, verbose_name="Альбом")
    text = models.TextField('Текст комментария')
    date = models.DateField('Дата', default=timezone.now)

    class Meta:
        verbose_name = "Комментарий"
        verbose_name_plural = "Коментарии"

    def __str__(self):
        return self.album.title + ' | ' + self.user.username


class View(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name="Пользователь")
    album = models.ForeignKey(Album, on_delete=models.CASCADE, verbose_name="Альбом")
    date = models.DateTimeField(default=timezone.now)

class Playlist(models.Model):
    audio = models.ForeignKey(Audio, on_delete=models.CASCADE, verbose_name="Аудио")
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name="Пользователь")
    date = models.DateTimeField(default=timezone.now)
    
    class Meta:
        verbose_name = "Плейлист"
        verbose_name_plural = "Плейлист"

    def __str__(self):
        return self.audio.title + ' | ' + self.user.username

