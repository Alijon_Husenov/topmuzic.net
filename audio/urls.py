from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('albums/', views.albums, name='albums'),
    path('album/<slug:slug>/', views.album_detail, name="album_detail_url"),
    path('genres/', views.genres, name="genres"),
    path('genres/<slug:slug>/', views.genre_detail, name='genre_detail_url'),
    path('search/', views.search, name='search'),
    # Comment
    path('albums/<slug:slug>/comment/', views.comment, name='comment'),
    path('albums/<slug:slug>/comment/<int:id>/', views.comment_delete, name='comment_delete'),
    # Auth
    path('login/', views.login_site, name="login"),
    path('logout/', views.logout_site, name="logout"),
    path('register/', views.register, name="register"),
    path('profile/', views.profile, name="profile"),
    path('profile/playlist/', views.playlist, name="playlist"),
    path('profile/playlist/<int:id>/delete/', views.remove_playlist, name="remove_playlist"),
    path('albums/<slug:slug>/audio/<int:id>/playlist/', views.add_playlist, name='add_playlist'),
    path('profile/clear-history/', views.clear_history, name="clear_history"),
    path('profile/edit/', views.user_edit, name="user_edit"),
]