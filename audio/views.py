from django.shortcuts import render, redirect, reverse
from .models import Genre, Album, Audio, Profile, Playlist
from django.db.models import Q, Count
from django.core.paginator import Paginator
from django.utils import timezone
from django.http import HttpResponse
from django.contrib.auth import authenticate, login, logout
from .forms import RegisterForm
from django.core.files.storage import FileSystemStorage


def index(request):
    albums = Album.objects.annotate(views=Count('view')).order_by('-views')[:12]
    genres = Genre.objects.all()[:5]
    context = {
        'genres': genres,
        'albums': albums
    }
    return render(request, 'audio/main/index.html', context)

def albums(request):
    albums = Album.objects.order_by('-created_at')
    paginator = Paginator(albums, 20)
    page_number = request.GET.get('page')
    albums = paginator.get_page(page_number)
    return render(request, 'audio/albums/albums.html', {'albums': albums})

def album_detail(request, slug):
    album = Album.objects.get(slug__exact=slug)
    if request.user.is_authenticated:
        if not album.view_set.filter(user = request.user).exists():
            album.view_set.create(user = request.user)
        else:
            album.view_set.get(user = request.user).date = timezone.now
    comments = album.comment_set.order_by('-date')
    paginator = Paginator(comments, 50)
    page_number = request.GET.get('page')
    comments = paginator.get_page(page_number)
    return render(request, 'audio/albums/album_detail.html', {'album': album, 'comments': comments})

def genres(request):
    genres = Genre.objects.order_by('title')
    paginator = Paginator(genres, 100)
    page_number = request.GET.get('page')
    genres = paginator.get_page(page_number)
    return render(request, 'audio/genres/genres.html', {'genres':genres})

def genre_detail(request, slug):
    genre = Genre.objects.get(slug__exact=slug)
    albums = genre.albums.order_by('-created_at')
    paginator = Paginator(albums, 20)
    page_number = request.GET.get('page')
    albums = paginator.get_page(page_number)
    return render(request, 'audio/genres/genres_detail.html', {'genre':genre, 'albums': albums})


def comment(request, slug):
    album = Album.objects.get(slug__exact=slug)
    if request.user.is_authenticated:
        if request.method == 'POST':
            album.comment_set.create(
                user = request.user,
                text = request.POST.get('text')
            )
    return redirect(reverse('album_detail_url', kwargs={'slug':slug}))

def comment_delete(request, slug, id):
    album = Album.objects.get(slug__exact=slug)
    comment = album.comment_set.get(id=id)
    if request.user.is_authenticated:
        if comment.user.id == request.user.id:
            if request.method == 'POST':
                comment.delete()
                return HttpResponse('Комментарий удалён')
        return HttpResponse('Вы не можете удалить чужой комментарий')
    return HttpResponse('Войдите в аккаунт, чтобы продолжить')
 
def search(request):
    query = request.GET.get('search')
    albums = Album.objects.filter(Q(title__icontains=query)).annotate(views=Count('view')).order_by('-views')[:12]
    audios = Audio.objects.filter(Q(title__icontains=query)).order_by('-date')
    paginator = Paginator(audios, 100)
    page_number = request.GET.get('page')
    audios = paginator.get_page(page_number)
    return render(request, 'audio/main/search.html', {'audios': audios, 'query': query, 'albums': albums})

def login_site(request):
    msg = None
    if request.method == 'POST':
        user = authenticate(username=request.POST.get('username'), password=request.POST.get('password'))
        if user is not None:
            login(request, user)
            return redirect('index')
        msg = 'Пользователь не найден'
    return render(request, 'audio/auth/login.html', {'msg': msg})

def logout_site(request):
    logout(request)
    return redirect('index')

def register(request):
    if request.user.is_authenticated:
        return redirect('index')
    if request.method == 'POST':
        form = RegisterForm(request.Post)
        if form.is_valid():
            user = form.save()
            profile = Profile()
            profile.user = user
            profile.save()
            login(request, user)
            form.save()
            return redirect('index')
    form = RegisterForm()
    return render(request, 'audio/register.html', {'form': form})

def profile(request):
    if request.user.is_authenticated:
        views = request.user.view_set.order_by('-date')
        return render(request, 'audio/auth/profile.html', {'views': views})
    return redirect('index')

def user_edit(request):
    if request.method == 'POST':
        request.user.first_name = request.POST.get('first_name')
        request.user.last_name = request.POST.get('last_name')
        request.user.save()
        request.user.profile.phone = request.POST.get('phone')
        if request.FILES.get('image', False):
            myfile = request.FILES['image']
            fs = FileSystemStorage()
            filename = fs.save(myfile.name, myfile)
            request.user.profile.avatar = filename
        request.user.profile.save()
        return redirect('profile')

def clear_history(request):
    if request.user.is_authenticated:
        if request.method == 'POST':
            request.user.view_set.all().delete()
    return redirect('profile')

def add_playlist(request, slug, id):
    audio = Audio.objects.get(id=id)
    if request.method == 'POST':
        if not request.user.playlist_set.filter(audio = audio).exists():
            playlist = Playlist()
            playlist.audio = audio
            playlist.user = request.user
            playlist.save()
    return redirect(reverse('album_detail_url', kwargs={'slug':slug}))

def remove_playlist(request, id):
    playlist = Playlist.objects.get(id=id)
    if request.method == 'POST':
        playlist.delete()
    return HttpResponse('Аудио Удалено')

def playlist(request):
    playlists = request.user.playlist_set.order_by('-date')
    return render(request, 'audio/playlist.html', {'playlists': playlists})

