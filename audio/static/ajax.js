$(function(){
    $(".delete").on('click', function(){
        var url = $(this).data('url')
        var comment = $(this).parent().parent()
        $.ajax({
            url: url,
            type: "POST",
            headers: {
                'X-CSRFToken': $('[name=csrfmiddlewaretoken]').val()
            },
            success: function(data){
                alert(data)
                comment.hide('slow', function(){ comment.remove() })
            },
            error: function(data){
                $('html').html(data['responseText'])
            }
        })
    })
})