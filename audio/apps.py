from django.apps import AppConfig


class AudioConfig(AppConfig):
    name = 'audio'
    verbose_name = 'Аудио'
